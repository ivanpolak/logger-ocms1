<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'prefix' => 'api/data',
    'middleware' => [
        'api',
        \WApi\ApiException\Http\Middlewares\ApiExceptionMiddleware::class,
    ]
], function () {
    Route::post('new', function () {
        $model = new \Applogger\Logger\Models\Data();
        $model->data = json_encode(request()->all());
        $model->save();
        return $model;
    });
});
