<?php namespace Applogger\Logger;

use Backend;
use System\Classes\PluginBase;

/**
 * logger Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'logger',
            'description' => 'No description provided yet...',
            'author'      => 'applogger',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Applogger\Logger\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'applogger.logger.some_permission' => [
                'tab' => 'logger',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'logger' => [
                'label'       => 'logger',
                'url'         => Backend::url('applogger/logger/data'),
                'icon'        => 'icon-leaf',
                'permissions' => ['applogger.logger.*'],
                'order'       => 500,
            ],
        ];
    }
}
